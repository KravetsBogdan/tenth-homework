let btn = document.querySelector('.tabs');

btn.addEventListener('click', (e) => {
    if (e.target.tagName === 'LI') {
        let active = document.querySelector('.active');
        active.classList.remove('active');
        e.target.classList.add('active')
    }
    let textChild = document.querySelectorAll('.tab-text');
    textChild.forEach(el => {
        if (e.target.id === el.dataset.text) {
            let isActive = document.querySelector('.is-active');
            isActive.classList.remove('is-active');
            el.classList.add('is-active');
        }
    })
})

